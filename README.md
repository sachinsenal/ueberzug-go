# ueberzug-go

A simple half-assed "port" (but not really) of ueberzug to Golang, aimed to replace w3m.

![](https://media.discordapp.net/attachments/361910177961738244/571994938787495936/unknown.png)

## Instructions

Refer to `ueberzug_test.go`

## Todo

- [ ] Handle soft translations 
- [ ] Alpha channels, maybe
- [ ] tview primitive
- [ ] Add more New* functions
